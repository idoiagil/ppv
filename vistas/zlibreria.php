<!---------------------------------------------------- NAVBAR --------------------------------------------->

<?php
function navbar()
{
?>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php"> <img src="images/logo.png" alt="">Pisos Pal Vicente</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>

            <?php
            session_start();
            if (isset($_SESSION['nick'])) {
            ?>
                <span style="margin-right: 10px;"> <a href="vistas/uModificarPerfil.php">Mi perfil</a> </span>
                <span style="margin-right: 10px;"> <a href="controlador/cerrarSesion.php">Cerrar Sesión</a> </span>

            <?php } else if (isset($_SESSION['adm'])) { ?>

                <span style="margin-right: 10px;"> <a href="vistas/aMensajes.php"> Panel </a> </span>
                <span style="margin-right: 10px;"> <a href="controlador/cerrarSesion.php">Cerrar Sesión</a> </span>
            <?php } else { ?>
                <span style="margin-right: 10px;"> <a href="vistas/uLogin.php"> Identificate </a></span>
            <?php } ?>
        </div>
    </nav>


<?php } ?>

<!---------------------------------------------------- NAVBAR ADMIN --------------------------------------------->

<?php
function navbarAdmin()
{
?>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php"> Pisos Pal Vicente</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <a href="aMensajes.php"> Panel </a>
            <a href="../controlador/cerrarSesion.php">Cerrar Sesión</a>
        </div>
    </nav>


<?php } ?>

<!---------------------------------------------------- NAVBAR USER --------------------------------------------->

<?php
function navbarUser()
{
?>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php"> Pisos Pal Vicente</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <a href="uModificarPerfil.php"> Mi perfil </a>
            <a href="../controlador/cerrarSesion.php">  Cerrar Sesión</a>
        </div>
    </nav>


<?php } ?>

<!---------------------------------------------------- MENU ADMIN --------------------------------------------->

<?php
function menuAdmin()
{
?>

    <div class="cuerporec">
        <ul>
            <li><a href="aMensajes.php">Mensajes</a></li>
            <li><a href="aPisos.php">Pisos</a></li>
            <li><a href="aGestionUsuarios.php">Usuarios</a></li>
            <li><a href="aLogo.php">Logo</a></li>
            <li><a href="aCrearAdmin.php">Administradores</a></li>
        </ul>
    </div>


<?php } ?>


<!---------------------------------------------------- MENU USER --------------------------------------------->

<?php
function menuUser()
{
?>
    <div class="cuerporec">
        <ul>
            <li><a href="uModificarPerfil.php"> Modifical perfil </a></li>
            <li><a href="uBaja.php"> Dar de baja </a></li>
            <li><a href="uMensajes.php"> Mensajes </a></li>
            <li><a href="uFavoritos.php"> Favoritos </a></li>

        </ul>
    </div>
<?php } ?>