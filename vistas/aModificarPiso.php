<?php
session_start();
if (isset($_SESSION['adm'])) {
?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Pisos - Modificar </title>
    </head>
    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarAdmin(); ?>
        <div class="bd">
            <?php menuAdmin(); ?>
            <div class="contenedoradmin">
                <form action="aModificarPiso.php" method="post">
                    <select name="titulo">
                        <?php
                        include("../modelo/piso.php");
                        $piso = new Piso();
                        $piso->mostrarPisosSelect();
                        ?>
                    </select>
                    <input type="submit" name="submit" value="Modificar">
                </form>
                <?php
                if (isset($_POST['titulo'])) {
                    $pisoElegido = $_POST['titulo'];
                    $piso->crearSesionPiso($pisoElegido);
                ?>
                    <div class="sign-up">
                        <div class="form">
                            <h2> Nuevo Piso </h2>
                            <form action="../controlador/vaModificarPiso.php" method="post" enctype="multipart/form-data">
                                <label>
                                    <span> Título </span>
                                    <input type="text" name="titulo" id="titulo" value="<?php echo $_SESSION['titulo']; ?>" required>
                                </label>
                                <label>
                                    <span> Número de habitaciones</span>
                                    <input type="number" name="habitaciones" id="habitaciones" value="<?php echo $_SESSION['habitaciones']; ?>" required>
                                </label>
                                <label>
                                    <span> Precio</span>
                                    <input type="number" name="precio" id="precio" value="<?php echo $_SESSION['precio']; ?>" required>
                                </label>
                                <label>
                                    <span> Descripcion </span>
                                    <input type="text" name="descripcion" id="descripcion" value="<?php echo $_SESSION['descripcion']; ?>" required>
                                </label>
                                <label>
                                    <span>Distancia al Montessori</span>
                                    <input type="number" name="distancia" id="distancia" value="<?php echo $_SESSION['distancia']; ?>" required>
                                </label>
                                <label>
                                    <span>Telefono </span>
                                    <input type="text" name="telefono" id="telefono" value="<?php echo $_SESSION['telefono']; ?>" required>
                                </label>
                                <label>
                                    <input type="hidden" name="imagenhidden" value="<?php echo $_SESSION['foto']; ?>">
                                </label>
                                <label for="imagen">
                                    <span>Imagen: </span>
                                    <input type="file" name="imagen" size="30" value="<?php echo $_SESSION['foto']; ?>">
                                </label>
                                <button type="submit" class="submit"> Modificar </button>
                            </form>
                        </div>
                    </div>
                <?php  } ?>
            </div>
        </div>
        <?php
    } else {
        echo "Usted no es administrador, por lo que no puede entrar en la página";
    }
        ?>
    </body>
    </html>