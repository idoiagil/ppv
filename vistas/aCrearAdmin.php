<?php
session_start();
if (isset($_SESSION['adm'])) {
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Crear administrador </title>
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarAdmin(); ?>
        <div class="bd">
            <?php menuAdmin(); ?>
            <div class="contenedoradmin">
                <div class="form">
                    <h2> Crear un nuevo administrador </h2>
                    <form action="../controlador/vaCrearAdmin.php" method="post">
                        <label>
                            <span> Administrador </span>
                            <input type="text" name="adm" required>
                        </label>
                        <label>
                            <span>Contraseña</span>
                            <input type="password" name="contra" required>
                        </label>
                        <button class="submit" type="submit"> Registrar </button>
                    </form>
                </div>
            </div>
        </div>
    <?php
} else {
    echo "Usted no es administrador, por lo que no puede entrar en la página";
}
    ?>
    </body>
    </html>