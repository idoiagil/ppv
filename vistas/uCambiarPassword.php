<?php
session_start();
if (isset($_SESSION['nick'])) {
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Cambiar contraseña </title>
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarUser(); ?>
        <div class="bd">
            <?php menuUser(); ?>
            <div class="contenedoradmin">
                <div class="sign-up">
                    <div class="form">
                        <form action="../controlador/vuCambiarPassword.php" method="post">
                            <label>
                                <span> Contraseña antigua </span>
                                <input type="password" name="aContra" id="aContra" required>
                            </label>
                            <label>
                                <span> Nueva contraseña </span>
                                <input type="password" name="nContra" id="nContra" required>
                            </label>
                            <label>
                                <span> Confirma la contraseña </span>
                                <input type="password" name="nContra2" id="nContra2" required>
                            </label>
                            <button class="submit" type="submit"> Entrar </button>
                        </form>
                    </div>
                </div>
            </div>
        <?php
    } else {
        echo "Usted no es usuario, por lo que no puede entrar en la página.";
    }
        ?>
    </body>
    </html>