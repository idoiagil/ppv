<!DOCTYPE html>
<html>

<head>
  <title> Regístrate </title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800&display=swap" rel="stylesheet">
  <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
  <link rel="stylesheet" href="../css/estilos.css">
</head>

<body>
  <?php include("zlibreria.php"); ?>
  <?php navbar(); ?>

  <div class="sign-up">
    <div class="form">
      <h2> Regístrate </h2>
      <form action="../controlador/vuRegistro.php" method="post">
        <label>
          <span> Nick </span>
          <input type="text" name="nick" id="nick" required>
        </label>
        <label>
          <span>Nombre</span>
          <input type="text" name="nombre" id="nombre" required>
        </label>
        <label>
          <span>Primer apellido</span>
          <input type="text" name="ape1" id="ape1" required>
        </label>
        <label>
          <span>Segundo apellido</span>
          <input type="text" name="ape2" id="ape2" required>
        </label>
        <label>
          <span>Edad</span>
          <input type="number" name="edad" id="edad" required>
        </label>
        <label>
          <span>Telefono </span>
          <input type="text" name="telefono" id="telefono" required>
        </label>
        <label>
          <span>Email</span>
          <input type="email" name="email" id="correo" required>
        </label>
        <label>
          <span>Contraseña</span>
          <input type="password" name="password" id="contra" minlength="6" required>
        </label>
        <label>
          <span>Confirmar contraseña</span>
          <input type="password" name="password" id="contra" minlength="6" required>
        </label>
        <button class="submit" type="submit"> Entrar </button>
        <a class="regis" href="login.php"> ¿Ya estás regístrado? Inicia sesión!</a>
      </form>
    </div>
  </div>
</body>
</html>