<?php
session_start();
if (isset($_SESSION['nick'])) {
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Responder mensaje</title>
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link href="../css/estilos.css" rel="stylesheet">
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarUser(); ?>
        <div class="bd">
            <?php menuUser(); ?>
            <?php
            $titulo = $_POST['titulo'];
            $de = $_POST['de'];
            $para = $_POST['para'];
            $asunto = $_POST['asunto'];
            ?>
            <div class="form">
                <form action="../controlador/vuMensaje.php" method="post">
                    <label>
                        <span><b>Para: </b> <?php echo $para; ?> </span>
                        <input type="hidden" value="<?php echo $para; ?>" name="para">
                    </label>
                    <label>
                        <span><b>De: </b> <?php echo $de; ?> </span>
                        <input type="hidden" value="<?php echo $de; ?>" name="de">
                    </label>
                    <label>
                        <span><b>Asunto: </b> </span>
                        <input type="text" value="<?php echo $asunto; ?>" name="asunto">
                    </label>
                    <label>
                        <span><b>Piso: </b> <?php echo $titulo; ?> </span>
                        <input type="hidden" value="<?php echo $titulo; ?>" name="titulo">
                    </label>
                    <label>
                        <span>Mensaje: </span>
                        <textarea name="mensaje" id="mensaje" required></textarea>
                    </label>
                    <button class="submit" type="submit"> Enviar </button>
                </form>
            </div>
        </div>
    </body>
<?php } else {
    echo "usted no es usuario";
} ?>

    </html>