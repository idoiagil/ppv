<?php 
$nombreLogo = $_FILES['logo']['name'];
$extension = pathinfo($nombreLogo, PATHINFO_EXTENSION);
$tamano = $_FILES['logo']['size'];
if ($tamano <= 30000000) {
    if ($extension == "jpeg" || $extension == "jpg" || $extension == "png" || $extension == "gif") {

        $directorio = '/proyecto/images/';
        move_uploaded_file($_FILES['logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $directorio . $nombreLogo);
    } else {
        echo "Solo se pueden subir imagenes de tipo jpeg, jpg, png o gif";
    }
} else {
    echo "El tamaño es demasiado grande";
}

include("../modelo/administrador.php");
$administrador = new Administrador();

if ($administrador ->insertarLogo($nombreLogo)) {
    header("Location: ../vistas/aLogo.php");
} else {
    header("Location: ../vistas/aLogo.php");    
} 

?>