<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PPV</title>
    <link href="css/bootstrap-4.4.1.css" rel="stylesheet">

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#"> <img src="images/logo.png" alt="">Pisos Pal Vicente</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>

            <?php
            session_start();
            if (isset($_SESSION['nick'])) {
            ?>
                <span style="margin-right: 10px;"> <a href="vistas/uModificarPerfil.php">Mi perfil</a> </span>
                <span style="margin-right: 10px;"> <a href="controlador/cerrarSesion.php">Cerrar Sesión</a> </span>

            <?php } else if (isset($_SESSION['adm'])) { ?>

                <span style="margin-right: 10px;"> <a href="vistas/aMensajes.php"> Panel </a> </span>
                <span style="margin-right: 10px;"> <a href="controlador/cerrarSesion.php">Cerrar Sesión</a> </span>
            <?php } else { ?>
                <span style="margin-right: 10px;"> <a href="vistas/uLogin.php"> Identificate </a></span>
            <?php } ?>
        </div>
    </nav>

    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleControls" data-slide-to="1"></li>
                        <li data-target="#carouselExampleControls" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="images/1920x500.gif" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Item 1 Heading</h5>
                                <p>Item 1 Description</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/1920x500.gif" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Item 2 Heading</h5>
                                <p>Item 2 Description</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/1920x500.gif" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Item 3 Heading</h5>
                                <p>Item 3 Description</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <hr>
    </div>

    <hr>


    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <?php

                    include "modelo/conexion_bbdd.php";

                    $conectar = new Conectar();
                    $conexion = $conectar->getconection();
                    $pisosPorPagina = 4; //articulos x pagina
                    $pagina = 1; //Pagina en la que estamos
                    if (isset($_GET['pagina'])) {
                        if ($_GET['pagina'] == 1) {
                            header("Location: index.php");
                        } else {
                            $pagina = $_GET['pagina'];
                        }
                    } else {
                        $pagina = 1;
                    }

                    $empezarDesde = ($pagina - 1) * $pisosPorPagina;
                    $sql1 = "SELECT * FROM `pisos`";
                    $result = $conexion->query($sql1);
                    $fow = $result->fetch_assoc();
                    $num = $result->num_rows;
                    $totalPaginas = ceil($num / $pisosPorPagina);
                    $sqlLimit = "SELECT * FROM `pisos` LIMIT $empezarDesde, $pisosPorPagina ";
                    $result = $conexion->query($sqlLimit);
                    $fow = $result->fetch_assoc();
                    $contador = 0;
                    do {
                        echo "<div class='col-sm-6 col-12'>";
                        echo "<div class='card' id='boxFoto' style='top: 15px;'>";
                        echo "<img class='card-img-top' src='images/". $fow['foto'] ."' alt='Card image cap'>";
                        echo "<div class='card-body'>";
                        echo "<h5 class='card-tittle'>" . $fow['titulo'] . "</h5>";
                        echo "<p class='card-text'>";
                        echo "Habitaciones: " . $fow['habitaciones'] . "<br>";
                        echo "Precio: " . $fow['precio'] . "<br>";
                        echo "Distancia al Montessori: " . $fow['distancia'] . "<br>";
                        echo "Telefono: " . $fow['telefono'] . "<br>";
                        echo "</p>";

                        echo "<form action='vistas/descripcionPiso.php' method='post'>";
                        echo "<input type='hidden' value='" . $fow['id'] . "' name='id'>";
                        echo "<input type='hidden' value='" . $fow['titulo'] . "' name='titulo'>";
                        echo "<input type='hidden' value='" . $fow['foto'] . "' name='foto'>";
                        echo "<input type='hidden' value='" . $fow['precio'] . "' name='precio'>";
                        echo "<input type='hidden' value='" . $fow['habitaciones'] . "' name='habitaciones'>";
                        echo "<input type='hidden' value='" . $fow['distancia'] . "' name='distancia'>";
                        echo "<input type='hidden' value='" . $fow['telefono'] . "' name='telefono'>";
                        echo "<input type='hidden' value='" . $fow['descripcion'] . "' name='descripcion'>";
                        echo "<button class='submit' type='submit' style='color: #fff; background-color: #007bff; border-color: #007bff; font-weight:400; color:#212529;text-align: center; border: 1px solid transparent; border-radius: 0.25rem;'> Descripción </button>";
                        echo "</form>";
                        echo "</div></div></div>";
                        $contador++;
                        if ($contador % 2 == 0) {
                            echo "</div><div class='row'>";
                        }
                    } while ($fow = $result->fetch_assoc()); ?>
                </div>

            </div>
        </div>
        <br><br><br><br>
        <hr>
        <div style="top: 15px;">
            <?php
            for ($i = 1; $i <= $totalPaginas; $i++) {
                echo "<a href='?pagina=" . $i . "'>" . $i . "</a> ";
            }
            ?>
        </div>


    </div>
    <br><br><br>
    </section style="top: 20px;">
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <h3>About Us</h3>
                        <hr>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, consequatur neque exercitationem distinctio esse! Cupiditate doloribus a consequatur iusto illum eos facere vel iste iure maxime. Velit, rem, sunt obcaecati eveniet id nemo molestiae. In.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, consequatur neque exercitationem distinctio esse! Cupiditate doloribus a consequatur iusto illum eos facere vel iste iure maxime. Velit, rem, sunt obcaecati eveniet id nemo molestiae. In.</p>
                    </div>
                    <div class="col-lg-6 col-12">
                        <h3>Latest News</h3>
                        <hr>
                        <div>
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="mt-0 mb-1">Heading 1</h4>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, quod temporibus veniam deserunt deleniti accusamus voluptatibus at illo sunt quisquam.
                                </div>
                                <a href="#"><img class="ml-3" src="images/75X.gif" alt="placeholder image"></a>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="mt-0 mb-1">Heading 2</h4>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, iure nemo earum quae aliquid animi eligendi rerum rem porro facilis.
                                </div>
                                <a href="#"><img class="ml-3" src="images/75X.gif" alt="placeholder image"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12 mt-md-0 mt-2">
                <h3>Contact Us</h3>
                <hr>
                <address>
                    <strong>MyStoreFront, Inc.</strong><br>
                    Indian Treasure Link<br>
                    Quitman, WA, 99110-0219<br>
                    <abbr title="Phone">P:</abbr> (123) 456-7890
                </address>
                <address>
                    <strong>Full Name</strong><br>
                    <a href="mailto:#">first.last@example.com</a>
                </address>
            </div>
        </div>
    </div>
    <footer class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p>Copyright © MyWebsite. All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.4.1.js"></script>
</body>

</html>