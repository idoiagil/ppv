<?php
include_once('conexion_bbdd.php');

class Usuario
{
    private $conexion;
    private $conectar;
    public function __construct()
    {
        $this->conectar = new conectar();
        $this->conexion = $this->conectar->getconection();
    }

    function insertarUsuario($nick, $nombre, $ape1, $ape2, $edad, $correo, $telefono, $contra)
    {

        //$con = $this->dameConexion();

        $sql = ("insert into `usuario`(nick, nombre, ape1, ape2, edad, correo, telefono, contra) values (?, ?, ?, ?, ?, ?, ?, ?)");

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('ssssisss', $nick, $nombre, $ape1, $ape2, $edad, $correo, $telefono, $contra);

        $stmt->execute();

        $stmt->close();

        return true;
    }

    function logear($nick, $contra)
    {

        session_start();

        //$sql = "SELECT * FROM $tbl_name WHERE Nombre = '$username'";

        $sql = "SELECT * FROM `usuario` WHERE nick = '$nick' and contra = '$contra'";

        $result = $this->conexion->query($sql);

        echo $result->num_rows;


        $row = $result->fetch_array();

        if ($result->num_rows > 0) {
            $_SESSION['nick'] = $row['nick'];
            $_SESSION['nombre'] = $row['nombre'];
            $_SESSION['ape1'] = $row['ape1'];
            $_SESSION['ape2'] = $row['ape2'];
            $_SESSION['edad'] = $row['edad'];
            $_SESSION['telefono'] = $row['telefono'];
            $_SESSION['correo'] = $row['correo'];
            $_SESSION['contra'] = $row['contra'];


            return true;
        } else {
            return false;
        }
    }


    function modUser($nick, $nombre, $ape1, $ape2, $edad, $correo, $telefono)
    {


        session_start();

        $nick1 = $_SESSION['nick'];
        $cst = $this->conexion->query("SELECT id FROM `usuario` WHERE nick = '$nick1'");
        $row = $cst->fetch_array();
        $id = $row['id'];

        $sql = ("update `usuario` set nick = ?, nombre = ?, ape1 = ?, ape2 = ?, edad = ?, correo = ?, telefono = ? where id = ?");

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('ssssissi', $nick, $nombre, $ape1, $ape2, $edad, $correo, $telefono, $id);

        $stmt->execute();


        if (mysqli_affected_rows($this->conexion) > 0) {
            $_SESSION['nick'] = $nick;
            $_SESSION['nombre'] = $nombre;
            $_SESSION['ape1'] = $ape1;
            $_SESSION['ape2'] = $ape2;
            $_SESSION['edad'] = $edad;
            $_SESSION['telefono'] = $telefono;
            $_SESSION['correo'] = $correo;
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }


    function modContra($nContra, $nick)
    {


        $sql = ("update `usuario` set contra = '$nContra' where nick = '$nick' ");

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('s', $nContra);

        $stmt->execute();

        if (mysqli_affected_rows($this->conexion) > 0) {
            $_SESSION['contra'] = $nContra;
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    function eliminarUser($nick)
    {
        $sqlF = ("delete from `favoritos` where nick = '$nick'");
        $stmtF = $this->conexion->prepare($sqlF);
        $stmtF->bind_param('s', $nick);
        $stmtF->execute();

        $sqlM = ("delete from `mensajes` where de = '$nick'");
        $stmtM = $this->conexion->prepare($sqlM);
        $stmtM->bind_param('s', $nick);
        $stmtM->execute();

        $sqlM1 = ("delete from `mensajes` where para = '$nick'");
        $stmtM1 = $this->conexion->prepare($sqlM1);
        $stmtM1->bind_param('s', $nick);
        $stmtM1->execute();

        $sql = ("delete from `usuario` where nick = '$nick' ");
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param('s', $nick);
        $stmt->execute();
        
        if (mysqli_affected_rows($this->conexion) > 0) {
            session_start();
            session_destroy();
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
        return true;
    }

    function favorito($nick, $idPiso)
    {


        $sql = "SELECT * FROM `favoritos` WHERE nick = '$nick' and idPiso = '$idPiso'";

        $result = $this->conexion->query($sql);


        //$row = $result->fetch_array();

        if ($result->num_rows > 0) {

            $query1 = ("delete from `favoritos` where nick = ? and idPiso = ?");

            $resultado = $this->conexion->prepare($query1);

            $resultado->bind_param('ss', $nick, $idPiso);

            $resultado->execute();

            $resultado->close();

            return true;
        } else {

            $sql1 = ("insert into `favoritos`(nick, idPiso) values (?, ?)");

            $stmt1 = $this->conexion->prepare($sql1);

            $stmt1->bind_param('ss', $nick, $idPiso);

            $stmt1->execute();

            $stmt1->close();

            return true;
        }
    }


    function muestrameFavoritosTabla($nick)
    {
        $sql = "SELECT * FROM `favoritos` WHERE nick = '$nick'";
        $result = $this->conexion->query($sql);
        $fow = $result->fetch_assoc();
        if ($result->num_rows == 0) {
        } else {
            do {
                echo "<tr>";
                echo "<td>" . $fow['idPiso'] . "</td>";
                echo "</tr>";
                echo "</form>";
            } while ($fow = $result->fetch_assoc());
        }
    }
}
