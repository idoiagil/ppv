<?php
include_once ('conexion_bbdd.php');

class Administrador
{

    private $conexion;
    private $conectar;

    public function __construct()
    {
        $this->conectar = new conectar();
        $this->conexion = $this->conectar->getconection();
    }

    function inicio($adm, $contra)
    {

        session_start();

        $sql = "SELECT * FROM `admin` WHERE adm = '$adm' and contra = '$contra'";
        $result = $this->conexion->query($sql);
        $row = $result->fetch_array();

        if ($result->num_rows > 0) {
            $_SESSION['adm'] = $row['adm'];
            $_SESSION['contra'] = $row['contra'];
            return true;
        } else {
            return false;
        }


    }

    function mostrarUsuariosSelect()
    {
        $sql = "SELECT * FROM `usuario`";
        $result = $this->conexion->query($sql);
        $fila = $result->fetch_assoc();

        do {
            echo "<option>" . $fila ['nick'] . "</option>";
        } while ($fila = $result->fetch_assoc());

    }

    function mostrarPisosSelect()
    {

        $sql = "SELECT * FROM `pisos`";
        $result = $this->conexion->query($sql);
        $fow = $result->fetch_assoc();

        do {
            echo "<option>" . $fow ['titulo'] . "</option>";
        } while ($fow = $result->fetch_assoc());

    }

    function eliminarUsuario($nick)
    {

        $sqlF = ("delete from `favoritos` where nick = '$nick'");
        $stmtF = $this->conexion->prepare($sqlF);
        $stmtF->bind_param('s', $nick);
        $stmtF->execute();

        $sqlM = ("delete from `mensajes` where de = '$nick'");
        $stmtM = $this->conexion->prepare($sqlM);
        $stmtM->bind_param('s', $nick);
        $stmtM->execute();

        $sqlM1 = ("delete from `mensajes` where para = '$nick'");
        $stmtM1 = $this->conexion->prepare($sqlM1);
        $stmtM1->bind_param('s', $nick);
        $stmtM1->execute();

        $sql = ("delete from `usuario` where nick = '$nick' ");
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param('s', $nick);
        $stmt->execute();


        if (mysqli_affected_rows($this->conexion) > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }


    }

    function eliminarPiso($titulo) {

        $sql = ("delete from `pisos` where titulo = '$titulo' ");

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('s', $titulo);

        $stmt->execute();


        if (mysqli_affected_rows($this->conexion) > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }


    }

    function nuevoAdm($nombre, $contra)
    {

        $sql = ("insert into `admin`(adm, contra) values (?, ?)");

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('ss', $nombre, $contra);

        $stmt->execute();

        $stmt->close();

        return true;

    }

    function insertarLogo($logo) {
        $sql = ("insert into `logo`(logo) values (?)");
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param('s', $logo);
        $stmt->execute();
        $stmt->close();
        return true;
    }

}