<?php

    define('HOST_NAME', 'localhost');
    define('DATABASE_NAME','ppv');
    define('USER','root');
    define('PASSWORD','');

    class conectar
    {
        private $hostname = HOST_NAME;
        private $database = DATABASE_NAME;
        private $user = USER;
        private $password = PASSWORD;
        private $conexion;

        function getconection()  {
            $this->conexion = new mysqli($this->hostname ,$this->user, $this->password ,$this->database);

            if ($this->conexion -> connect_error) {
                die("Connection failed: " . $this->conexion->connect_error);
            }
            return $this->conexion;
        }

    }
?>
