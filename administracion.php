<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Iniciar Sesión </title>
    <link href="css/bootstrap-4.4.1.css" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
</head>

<body>

    <!-- NAVBAR-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#"> <img src="images/logo.png" alt="">Pisos Pal Vicente</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>

            <?php
            session_start();
            if (isset($_SESSION['nick'])) {
            ?>
                <span style="margin-right: 10px;"> <a href="vistas/user/modUser.php">Mi perfil</a> </span>
                <span style="margin-right: 10px;"> <a href="controlador/cerrarSesion.php">Cerrar Sesión</a> </span>

            <?php } else if (isset($_SESSION['adm'])) { ?>

                <span style="margin-right: 10px;"> <a href="vistas/admin/mensajesAdmin.php"> Panel </a> </span>
                <span style="margin-right: 10px;"> <a href="controlador/cerrarSesion.php">Cerrar Sesión</a> </span>
            <?php } else { ?>
                <span style="margin-right: 10px;"> <a href="vistas/login.php"> Identificate </a></span>
            <?php } ?>
        </div>
    </nav>

    <!-- INICIO SESION ADMIN -->
    <div class="principal">
        <div class="cont">
            <div class="form">
                <h2> Admin </h2>
                <form action="controlador/vaAdministracion.php" method="post">
                    <label>
                        <span> Administrador </span>
                        <input type="text" name="adm" required>
                    </label>
                    <label>
                        <span>Contraseña</span>
                        <input type="password" name="contra" required>
                    </label>
                    <button class="submit" type="submit"> Entrar </button>
                </form>
            </div>
        </div>
    </div>


</body>

</html>